import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Schedule from "@material-ui/icons/Schedule";
import List from "@material-ui/icons/List";
import NoteIcon from '@material-ui/icons/MusicNote';
import ColorIcon from '@material-ui/icons/BorderColor';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Button from "components/CustomButtons/Button.js";






// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import NavPills from "components/NavPills/NavPills.js";

import styles from "assets/jss/nextjs-material-kit/pages/componentsSections/pillsStyle.js";

const useStyles = makeStyles(styles);

export default function SectionPills() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <div className={classes.container}>
        <div id="navigation-pills">
          <div className={classes.title}>
            <h3>Come Prepared!</h3>
          </div>
          <div className={classes.title}>
            <h3>
              <small>Make, create, or celebrate in the coming days, and share with us at the gathering!</small>
            </h3>
          </div>
          {/* <GridContainer> */}
            {/* <GridItem xs={12} sm={12} md={8} lg={6}> */}
              <NavPills
                color="primary"
                tabs={[
                  {
                    tabButton: "Make a Song",
                    tabIcon: NoteIcon,
                    tabContent: (
                      <span>
                        <p>
                          Set some of the Sacred Verses to melody! Here will be a collection of Writings about Ridván:
                        </p>
                        <Button color="primary" href="/song">Yes! I would love to make a song!</Button>

                      </span>
                    )
                  },
                  {
                    tabButton: "Visual Arts",
                    tabIcon: ColorIcon,
                    tabContent: (
                      <span>
                        <p>
                          Paint or draw something beatiful, and bring it with you! There are also have some activities including coloring sheets.
                        </p>
                        <Button color="primary" href="/activities">Take me to the coloring sheets!</Button>

                      </span>
                    )
                  },
                  {
                    tabButton: "Memorization",
                    tabIcon: FavoriteIcon,
                    tabContent: (
                      <span>
                        <p>
                          Memorize some Verses or Tablets about Ridván. 
                        </p>
                        <Button color="primary" href="/memorize">I'll have it Ready by Ridván!</Button>
                      </span>
                    )
                  },
                  {
                    tabButton: "Cooking",
                    tabIcon: List,
                    tabContent: (
                      <span>
                        <p>
                         Creations in the Kitchen! Here are a couple recipes:
                        </p>
                        <Button color="primary" href="/kitchen">Bring out the Cookbook!</Button>

                      </span>
                    )
                  }
                ]}
              />
            {/* </GridItem> */}
            {/* <GridItem xs={12} sm={12} md={12} lg={6}>
              <NavPills
                color="rose"
                horizontal={{
                  tabsGrid: { xs: 12, sm: 4, md: 4 },
                  contentGrid: { xs: 12, sm: 8, md: 8 }
                }}
                tabs={[
                  {
                    tabButton: "Dashboard",
                    tabIcon: Dashboard,
                    tabContent: (
                      <span>
                        <p>
                          Collaboratively administrate empowered markets via
                          plug-and-play networks. Dynamically procrastinate B2C
                          users after installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence
                          without revolutionary ROI. Collaboratively
                          administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence
                          without revolutionary ROI. Collaboratively
                          administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p>
                      </span>
                    )
                  },
                  {
                    tabButton: "Schedule",
                    tabIcon: Schedule,
                    tabContent: (
                      <span>
                        <p>
                          Efficiently unleash cross-media information without
                          cross-media value. Quickly maximize timely
                          deliverables for real-time schemas.
                        </p>
                        <br />
                        <p>
                          Dramatically maintain clicks-and-mortar solutions
                          without functional solutions. Dramatically visualize
                          customer directed convergence without revolutionary
                          ROI. Collaboratively administrate empowered markets
                          via plug-and-play networks. Dynamically procrastinate
                          B2C users after installed base benefits.
                        </p>
                      </span>
                    )
                  }
                ]}
              />
            </GridItem> */}
          {/* </GridContainer> */}
        </div>
      </div>
    </div>
  );
}
