import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
import PublicIcon from '@material-ui/icons/Public';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';




// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InfoArea from "components/InfoArea/InfoArea.js";

import styles from "assets/jss/nextjs-material-kit/pages/landingPageSections/productStyle.js";

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 className={classes.title}>The Divine Springtime has come!</h2>
          <h3 className={classes.description}>
          Please join the George family for an online video conference celebration of the 9th day of Ridván! Here is how you can participate.
          </h3>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Join the Video Call from this Page!"
              description="The button to join the meeting will be front and center on the Holy Day!"
              icon={PublicIcon}
              iconColor="info"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Prepare to Share!"
              description="Create something to share with everyone, a song, visual arts, an experience celebrating! When we gather we will have space for everyone to share what they have made."
              icon={AutorenewIcon}
              iconColor="success"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Devotions Start at 10:00am EST"
              description="We will start with devotions promptly at 10:00am EST. Your welcome to stop by a bit early."
              icon={AccessAlarmIcon}
              iconColor="danger"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
