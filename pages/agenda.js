import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import Camera from "@material-ui/icons/Camera";
import Palette from "@material-ui/icons/Palette";
import Favorite from "@material-ui/icons/Favorite";
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import NavPills from "components/NavPills/NavPills.js";
import Parallax from "components/Parallax/Parallax.js";

import profile from "assets/img/garden/flowers1.jpg";


import styles from "assets/jss/nextjs-material-kit/pages/profilePage.js";

const useStyles = makeStyles(styles);

export default function ProfilePage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );

  let [zone, setZone] = React.useState('10am South Carolina Time')
  let [counter, setCounter] = React.useState(0);
  let timezones = [
    '9am Chicago time',
    '4am Mafikeng time',
    '5pm Kuwait time',
    '10am Georgia time',
    '7am Phoenix time',
    '10pm Shanghai time',
    '10am New York time',
  ]
  let numberEr = (el) => el.replace(/[^0-9]/g, '')

  React.useEffect(() => {
    const interval = setInterval(() => {
      setZone(timezones[counter]);
      setCounter(counter => counter + 1);
      if (counter + 1 == timezones.length) {
        setCounter(0);
        // clearInterval(inst); // uncomment this if you want to stop refreshing after one cycle
      }
    }, 8000);
    return () => clearInterval(interval);
  }, [counter]);

  const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);
  return (
    <div>
      <Header
        color="transparent"
        brand="Ridvan.org"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white"
        }}
        {...rest}
      />
      <Parallax small filter image={require("assets/img/landing-bg2.jpg")} />
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div>
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={6}>
                <div className={classes.profile}>
                  <div>
                    <img src={profile} alt="..." className={imageClasses} />
                  </div>
                  <div className={classes.name}>
                    <h2 className={classes.title}>Ridván Celebration Schedule</h2>
                    <h3><span
                      style={{
                        fontWeight: "bold"
                      }}
                    >{zone}</span>, April 28th, 2020</h3>
                  </div>
                </div>
              </GridItem>
            </GridContainer>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={8} className={classes.navWrapper}>
                <NavPills
                  alignCenter
                  color="primary"
                  tabs={[
                    {
                      tabButton: `${numberEr(zone)}:00 Devotions`, tabIcon: Camera,
                      tabContent: (
                        <GridContainer justify="center">
                          <GridItem xs={12} sm={12} md={4}>
                            <h2>Devotions</h2>
                            <div>We want devotions to feel like entering a House of Worship!</div>                          
                            <div>Please enter quietly, we will have a few minutes at the start for everyone to enter and be welcomed, if you would like to share a prayer please raise your hand or push the raise hand button.</div>
                            <div>We will go down the list in order, pausing in silence for about 30 seconds after each prayer.</div>
                          </GridItem>
                        </GridContainer>
                      )
                    },
                    {
                      tabButton: `${numberEr(zone)}:15 Sharing`,
                      tabIcon: Palette,
                      tabContent: (
                        <GridContainer justify="center">
                          <GridItem xs={12} sm={12} md={4}>
                            <h2>Sharing Our Ridván</h2>
                            <div>We will go around the room to each family and introduce ourselves and share what we have been doing and making to celebrate Ridvan.</div>
                            <h4>We have some resources and suggestions provided, but its not limited, whatever your contribution, it's welcome!</h4>
                          </GridItem>
                          <GridItem>
                            <h4>Resources:</h4>
                            <Button
                              color="primary"
                              href="/song">
                              Make a song
                            </Button>
                            <Button
                              color="primary"
                              href="/kitchen">
                              Cooking
                            </Button>
                            <Button
                              color="primary"
                              href="/story">
                              Tell a Story
                            </Button>
                            <Button
                              color="primary"
                              href="/memorize">
                              Memorization
                            </Button>
                            <Button
                              color="primary"
                              href="/activities">
                              Activities
                            </Button>
                          </GridItem>
                        </GridContainer>
                      )
                    },
                    {
                      tabButton: `${numberEr(zone)}:30 Break out rooms`,
                      tabIcon: Favorite,
                      tabContent: (
                        <GridContainer justify="center">
                          <GridItem xs={12} sm={12} md={4}>
                            <h2>Break Out Rooms</h2>
                            <div>There will be 4 different rooms to visit in afterward.</div>
                            <h3>Devotional Room</h3>
                            <div>A devotional for those who would like to continue praying.</div>
                            <h3>Game Room</h3>
                            <div>We have a couple of games to play together.</div>
                            <h3>Reading The Ridvan Letter</h3>
                            <div>Room for reading and studying the Ridvan letter.</div>
                            <h3>Children's Room</h3>
                            <div>Children can come and learn a song and do a craft.</div>
                            <br />
                          </GridItem>
                        </GridContainer>
                      )
                    }
                  ]}
                />
              </GridItem>
              <GridItem>
                <h3>To the Participants:</h3>
                <p>Allah'u'Abha, thanks so much for contributing to the Holy Day Celebration!</p>
                <p>Firstly, every person who enters needs to feel welcome. Wherever able, greet each person lovingly. ‘Abdu’l-Bahá wrote </p>
                <p><i>"O ye friends! Fellowship, fellowship! Love, love! Unity, unity!—so that the power of the Bahá’í Cause may appear and become manifest in the world of existence. My thoughts are turned towards you, and my heart leaps within me at your mention. Could ye know how my soul glows with your love, so great a happiness would flood your hearts as to cause you to become enamored with each other." -‘Abdu’l-Bahá
              </i>
                </p>
                <p><i>"The divine friends must be attracted to and enamored of each other and ever be ready and willing to sacrifice their own lives for each other. Should one soul from amongst the believers meet another, it must be as though a thirsty one with parched lips has reached to the fountain of the water of life, or a lover has met his true beloved." -‘Abdu’l-Bahá
                </i>
                </p>
                <p>
                  <i>"Servitude to God lieth in servitude to the friends. One must be the essence of humility and the embodiment of meekness. One must become evanescence itself and be healed of every disease of the self, in order to become worthy of thraldom to the Threshold of the Almighty." (From a Tablet of ‘Abdu’l-Bahá—translated from the Persian)
                  </i>
                </p>
                <p>
                Secondly, this space should be focused as much as possible on the gathering and not the technology, wherever possible we want to avoid having conversations about how to do this and how to do that. If they are necessary, keep them as brief as possible. We won't be able to help everyone learn how to use the all features of the software, but if there is a barrier to participation we can remove without halting the rest of the participants, great.
                </p>
                <p>
                Everyone is welcome to help out, if someone you know is having trouble, help them out on the side, not in meeting itself. Text, email, or call, and help them through the problem outside of the public space. Hopefully most people are experts these days.
                </p>
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
