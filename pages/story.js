import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import SectionTabs from "pages-sections/Components-Sections/SectionTabs.js";
import Card from 'components/Card/Card.js'
import CardBody from 'components/Card/CardBody.js'
import CardHeader from 'components/Card/CardHeader.js'
import styles from "assets/jss/nextjs-material-kit/pages/landingPage.js";

// Sections for this page
import ProductSection from "pages-sections/LandingPage-Sections/ProductSection.js";
import TeamSection from "pages-sections/LandingPage-Sections/TeamSection.js";
import WorkSection from "pages-sections/LandingPage-Sections/WorkSection.js";
import SectionPills from "pages-sections/Components-Sections/SectionPills.js";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function Prepare(props) {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Ridvan.org"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
    <Parallax filter responsive image={require("assets/img/story.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>Tell a Story</h1>
              <h3>
                Share a story with your family, or at the celebration!
              </h3>
              
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
   {/* <SectionTabs /> */}

        <div className={classes.container}>
          <br />
          <div style={{color:'black'}}>
            <div>Here are a couple stories you could practice, or pick another.</div>
          <h3 >Contents</h3>
          <ol>
            <li>The Story of The Declaration of Bahá'u'lláh</li>
            <li>The Story of the Conference of Badasht</li>
          </ol>


          </div>
          <GridContainer>
          <GridItem >

        <Card >
      <CardHeader color="primary">
        <h3 className="springTime">The Declaration of Bahá'u'lláh</h3>
      </CardHeader>
      <CardBody>
       <h3>'Abdul-Baha tells the <a href="www.bahai.org/r/340852552" target="_blank">story</a>:</h3> 
       <p>From the beginning of His childhood Bahá’u’lláh was possessed of such astonishing qualities, signs, and utterances as to amaze every soul. All the dignitaries of Persia would say: “This youth is wrought of a rare substance”, and everyone, even the enemies and the envious, bore witness to His knowledge, grace, wisdom, understanding, intelligence, and perception. Among other things, it was acknowledged by all that He had neither entered a school nor received a religious education. Nonetheless, His knowledge and perfections were well recognized. The learned men of Persia would submit to Him the difficult questions that perplexed their minds, and He would resolve them. To this day, and in spite of their hostility, the dignitaries of Persia bear witness to this matter. </p>
       <p>
In sum, no one, whether in Persia or even throughout the East, denies Bahá’u’lláh’s knowledge, perfection, greatness, and ability. At most they claim that this Man subverted the foundations of the Law of God, that by means of His shrewdness, intelligence, knowledge, wisdom, eloquence, and sagacity He led astray a vast multitude, and that He thus undermined the perspicuous religion of God. But they do not deny His greatness.</p>
        <p>
Thus, from the very beginning of the Revelation of the Báb, the believers were humble and lowly before Bahá’u’lláh, looked to Him for guidance, and were drawn to Him with a heartfelt attraction. But at Badasht the greatness and majesty of Bahá’u’lláh were manifested to a further degree. There, a number of believers developed a particular devotion and became wholly attracted to Him. Whoever met Him and heard His words would be transformed and enthralled, and could do naught but surrender his will and become aflame with the fire of the love of God. </p>
<p>
During His final days in Ṭihrán, prior to the journey to Baghdád, some of the believers, such as Muḥammad Taqí Khán, Sulaymán Khán, Jináb-i-‘Aẓím, Mírzá ‘Alí-Muḥammad, Mullá ‘Abdu’l-Fattáḥ, and Mírzá ‘Abdu’l-Vahháb—all of whom were to be later martyred—as well as Mírzá Ḥusayn Kirmání and many other souls, perceived that Bahá’u’lláh occupied a transcendent station and became convinced that He was a Manifestation of God. Bahá’u’lláh had composed an ode from which the fragrance of a heavenly station could be perceived, the opening of which reads: “’Tis from Our rapture that the clouds of realms above are raining down.” All the friends would recite that ode with the utmost fervour and attraction, and all accepted its purport—not a soul voiced an objection. That ode was indeed most enthralling. </p>
<p>
The first person who recognized the sublimity and holiness of Bahá’u’lláh and became certain that He would manifest a momentous Cause was Mullá ‘Abdu’l-Karím-i-Qazvíní, whom the Báb had named Mírzá Aḥmad. He was the intermediary between the Báb and Bahá’u’lláh and was aware of the truth of the matter.</p>
<p>
After coming to Baghdád from Persia, Bahá’u’lláh declared to a certain extent the nature of His mission in the ninth year after the appearance of the Báb, and became known among the friends as the appearance of Ḥusayn. For the people of Persia believed that the appearance of the promised Mahdi must be followed by that of Ḥusayn, that is, of Imám Ḥusayn the martyr, to whom they are indeed most attached and bear the greatest allegiance. </p>
<p>
Now, in all His Books and Scriptures, the Báb heralded that which was to transpire in the year nine. Among them, there abound expressions such as: “In the year nine ye shall attain unto all good.” And such statements as “In the year nine ye shall …”, and “Then ye shall …”, and “Then ye shall …” are numerous. Likewise, He says: “Wait thou until nine will have elapsed from the time of the Bayán. Then exclaim: ‘Blessed, therefore, be God …’” In sum, the tidings of the Báb regarding the year nine are such as to defy all description. Nevertheless certain souls faltered, among them Mírzá Yaḥyá, Siyyid Muḥammad-i-Iṣfahání, and a few others. The Sermon of Salutations (Khuṭbiy-i-Ṣalavát) was revealed in the year nine, and likewise the commentary on the verse of the Qur’án “All food was allowed to the children of Israel except what Israel forbade itself” (Lawḥ-i-Kullu’ṭ-Ṭa‘ám) issued forth in that same year. </p>
<p>
Perceiving the covert rebellion of Mírzá Yaḥyá and others, Bahá’u’lláh journeyed alone to Sulaymáníyyih and was absent for two years. During that time, Mírzá Yaḥyá was acting with utmost caution behind a veil of concealment and, fearing the attention of the General Consul of Persia in Baghdád, disguised himself, took the name of Ḥájí ‘Alí, and engaged in selling shoes and plaster in Baṣrah and in Súqu’sh-Shuyúkh in the vicinity of Baghdád. The Cause became entirely quiescent, the Call ceased to be heard, and all name and trace thereof well-nigh vanished. </p>
<p>
During His sojourn in Sulaymáníyyih, Bahá’u’lláh penned a number of works, among them certain prayers of which copies are still extant, and certain epistles on mystical wayfaring addressed to the doctors and the learned men of Islam, which are likewise still extant. In those epistles certain teachings are expounded, among them words to this effect: “Were it not contrary to the perspicuous Law of God, I would have given my would-be murderer to be my heir. But what am I to do—I have no worldly possessions, nor hath it been thus decreed by His sovereign will.”</p>
<p>In any event, all the doctors and learned men of Sulaymáníyyih attested to the knowledge, attainments, and perfections of Bahá’u’lláh and developed an affection for His person; that is, they would say that this Man was unique and ranked among the chosen ones of God. </p>
<p>
When Bahá’u’lláh returned from Sulaymáníyyih, He illumined Baghdád with His light: The call of God was raised anew and a tumult arose in Persia. In Baghdád Bahá’u’lláh stood firm before all peoples. The government of Persia was extremely hostile in those days, and all were seeking by every means to cause Him suffering and to bring Him to harm. At last the Persian government, having grown alarmed at His influence, said: “Baghdád is close to Persia and is a place of passage for the Persians. Thus, in order to put out this fire Bahá’u’lláh must be banished to a distant land.” The Persian government then petitioned the Ottoman government, and Bahá’u’lláh was as a result transferred with all due honour out of Baghdád. Leaving the city, Bahá’u’lláh went to the garden of Najíb Páshá and resided there for twelve days. During that time many people, both high and low, and even the Governor and a number of other officials, attained His blessed presence. These are the twelve days of Riḍván.</p>
<p>
In any event, it was by means of hints and allusions that Bahá’u’lláh first declared His mission during those twelve days. Certain among the friends grasped His intent, but others did not fully understand. At last Bahá’u’lláh came to Constantinople and the Súrih of pilgrimage was revealed, wherein the instruction is given to circumambulate the House of Baghdád. In that Súrih the Cause is openly manifest, but the phrase “He Whom God shall make manifest” does not appear. </p>
<p>
Subsequently, the Persian government caused Bahá’u’lláh to be further banished to Adrianople. From there numerous Tablets were revealed day and night to the effect that “Since We have been expelled from our homeland and banished from Baghdád to a remote place, that the fire of the love of God might be quenched, the lamp of guidance extinguished, the banner of God hauled down, and the call of the True One silenced, We have therefore chosen to fully reveal the Cause, manifest the proof, raise the call, and hoist the banner of the Cause of God, that all may see that this persecution, enmity, banishment, and exile has only deepened the influence of the Word of God, that the fame of the Cause has been noised abroad, and that the tidings of the advent of the Kingdom of God have reached unto both East and West.” This universal declaration took place in the year 1280. All the friends, with the exception of Yaḥyá and a few of his followers, became firm and devoted believers, and from Adrianople Tablets would ceaselessly flow to Persia. </p>
<p>
This is an account, in summary form, of the Declaration of Bahá’u’lláh.</p>
      </CardBody>
    </Card>   
    </GridItem>
    <GridItem>
      <Card>
        <CardHeader color="primary">
          <h3>The Conference of Badasht</h3>
        </CardHeader>
        <CardBody>
        <h3>'Abdul-Baha tells the <a href="www.bahai.org/r/037805626" target="_blank">story</a>:</h3> 

        <p>In brief, what happened is the following. Those were the early days of the Cause and no one was informed of the divine teachings. All followed the law of the Qur’án and regarded warfare, retribution, and retaliation as permissible. In Qazvín, Ḥájí Mullá Taqí8 launched an attack from the pulpit and condemned those two resplendent stars, Shaykh Aḥmad-i-Aḥsá’í and Siyyid Káẓim-i-Rashtí. He cursed and reviled them vehemently, saying: “This affair of the Báb, which is unmitigated error, is a hellish fire that has blazed forth from the grave of Shaykh Aḥmad and Siyyid Káẓim.” In sum, he uttered the most brazen words and repeatedly hurled insults and invective at them. </p>
        <p>A believer from Shíráz9 was present at his sermon and heard it with his own ears. As he was unaware of the divine teachings that were yet to be promulgated and the principles upon which the religion of God was to be established, he concluded that it behoved him to act according to the law of the Qur’án, and thus he set out to settle the score. He went before dawn to the mosque of the said Ḥájí Mullá Taqí and concealed himself in an alcove. When at dawn Ḥájí Mullá Taqí came to the mosque, that individual stabbed him in the back and in the mouth with a spear-tipped cane. Ḥájí Mullá Taqí fell to the ground and his assailant fled. When the people arrived, they saw the cleric lying dead. </p>
        <p>A great tumult erupted and throughout the city a hue and cry was raised. The dignitaries of the town decided in concert that the assassins were Shaykh Rasúl-i-‘Arab and two other individuals, whom they viewed as being among the associates of Ṭáhirih. They immediately arrested these three individuals, and Ṭáhirih herself was subjected to severe restrictions. When that man from Shíráz saw that others had been apprehended in his place, he felt it unfit to remain silent and came of his own accord to the seat of the government to declare that Shaykh Rasúl and his friends were entirely innocent of the wrongful accusations levelled against them, and that he himself was the murderer. He described in full detail what had transpired, and confessed, saying: “These people are innocent: Set them free, for I am the guilty one and it is I who must be punished.” They arrested him but kept the others captive. </p>
        <p>
Briefly, they brought these four people from Qazvín to Ṭihrán. No matter how much that man from Shíráz protested that it was he who was guilty and that the others were entirely innocent—explaining that he had committed the crime because the victim had openly cursed and reviled his master from the pulpit and that, outraged and unable to contain himself, he had therefore stabbed him in the mouth with a spearhead—no one listened. To the contrary, Ḥájí Mullá Taqí’s son clamoured before the ministers of the government for the death of all four. Ṣadru’l-‘Ulamá, who was the head of the clergy, sought an audience with the Sháh and said: “Ḥájí Mullá Taqí was an illustrious man, highly renowned in the eyes of all and deeply revered by the people of Qazvín. In avenging the murder of such a man, a single individual is of no consequence. All four men must be turned over to the heirs of Mullá Taqí and delivered to Qazvín, that they may be executed in that city and that its inhabitants may thus be placated.” Out of regard for Ṣadru’l-‘Ulamá and the people of Qazvín, the Sháh gave word that all four could be executed.</p>
        <p>The man from Shíráz, seeing that the others had not been released in spite of his own arrest, escaped on a snowy night and went to the house of Riḍá Khán. Together they made a pact and departed for Shaykh Ṭabarsí, where they both met with martyrdom. As to Shaykh Rasúl and his friends, they were taken to Qazvín, where the populace fell upon them and killed them in the most horrendous manner. </p>
        <p>s a result, Ṭáhirih met with the greatest hardship. No one would associate with her, and all her relatives—even her husband and two sons—showed the greatest enmity and would oppress and revile her. Bahá’u’lláh dispatched Áqá Hádíy-i-Qazvíní from Ṭihrán and, by an elaborate stratagem, arranged for Ṭáhirih to be rescued from Qazvín and brought directly to the private quarters of His house. At first no one knew of this, but when some within the inner circle of the believers were informed, they came to see her. I was a child, sitting on her lap and being held in her arms. The curtain was drawn, and those believers were seated in an adjoining room while she was speaking. The purport of her discourse, which was supported by a range of arguments, as well as by the Qur’án and the traditions of the Prophet, was that in every age an illustrious and distinguished Individual must be the focal Centre of the circle of guidance, the Pole Star of the firmament of the most excellent Law of God, and a perspicuous Leader; that all may defer to Him; and that in this day that illustrious and distinguished Individual is the Báb, Who has manifested Himself. Although her speech was eloquent, yet when she perceived that Bahá’u’lláh was to raise another call and shine forth with another radiance, she became even more enkindled and reached a state that can hardly be described. She forsook all patience and composure and well-nigh rent asunder the veil of concealment. Night and day she would at turns speak forth and cry out, laugh aloud, and weep bitterly.</p>
        <p>
Later Bahá’u’lláh sent her with a number of believers towards Badasht. Their first stop was a beautiful and verdant garden. Ṭáhirih and the friends arrived there and were later joined by Bahá’u’lláh, Who rested the night there. In the morning He sent Ṭáhirih and the friends with ample provisions to Badasht. After a few days, Bahá’u’lláh Himself went there. When He reached Badasht, Quddús had returned from Khurásán and he, too, came to Badasht, but he remained concealed</p>
        <p>
In Badasht there was a field with a stream running through it and gardens to either side. Quddús remained concealed in one of the gardens, and Ṭáhirih resided in the other. A tent had been pitched for Bahá’u’lláh on that field, and the other believers were also housed in tents erected on the same field. In the evenings Bahá’u’lláh, Quddús, and Ṭáhirih would meet. Bahá’u’lláh made a solemn agreement with them that the truth of the Cause would be proclaimed at Badasht, but no specific day was designated. </p>
        <p>
Then, by chance, Bahá’u’lláh fell ill. As soon as he was informed, Quddús emerged from his concealment and entered Bahá’u’lláh’s tent. Ṭáhirih sent a message saying: “Either bring Bahá’u’lláh to the garden where I reside or I will come myself.” Quddús said: “Bahá’u’lláh is unwell and cannot come”, which was a signal. Ṭáhirih, seizing upon the opportunity, arose and, unveiled, came forth from the garden. She proceeded towards the tent of Bahá’u’lláh crying out and proclaiming: “I am the Trumpet-blast; I am the Bugle-call!”—which are two of the signs of the Day of Resurrection mentioned in the Qur’án. Calling out in this fashion, she entered the tent of Bahá’u’lláh. No sooner had she entered than Bahá’u’lláh instructed the believers to recite the Súrih of the Event from the Qur’án, a Súrih that describes the upheaval of the Day of Resurrection</p>
        <p>
In such wise was the Day of Resurrection proclaimed. The believers were seized with such fear and terror that some fled, others remained bewildered and dumbfounded, and still others wept and lamented. Some were so dismayed that they fell ill, and Ḥájí Mullá Ismá‘íl was so overcome with fear and terror that he cut his own throat. But after a few days, peace and composure were regained and the confusion and anxiety were dispelled. Most of those who had fled became steadfast again, and the episode of Badasht drew to a close.</p>
        </CardBody>
      </Card>
    </GridItem>
          </GridContainer>
        </div>
      </div>
      <Footer />
    </div>
  );
}
