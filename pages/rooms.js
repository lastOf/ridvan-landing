import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import Camera from "@material-ui/icons/Camera";
import Palette from "@material-ui/icons/Palette";
import Favorite from "@material-ui/icons/Favorite";
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import NavPills from "components/NavPills/NavPills.js";
import Parallax from "components/Parallax/Parallax.js";

import profile from "assets/img/garden/flowers1.jpg";



import styles from "assets/jss/nextjs-material-kit/pages/profilePage.js";

const useStyles = makeStyles(styles);

export default function ProfilePage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );

  let [zone, setZone] = React.useState('10am South Carolina Time')
  let [counter, setCounter] = React.useState(0);
  let timezones = [
    '9am Chicago time',
    '4am Mafikeng time',
    '5pm Kuwait time',
    '10am Georgia time',
    '7am Phoenix time',
    '10pm Shanghai time',
    '10am New York time',
  ]
  let numberEr = (el) => el.replace(/[^0-9]/g, '')

  React.useEffect(() => {
    const interval = setInterval(() => {
      setZone(timezones[counter]);
      setCounter(counter => counter + 1);
      if (counter + 1 == timezones.length) {
        setCounter(0);
        // clearInterval(inst); // uncomment this if you want to stop refreshing after one cycle
      }
    }, 8000);
    return () => clearInterval(interval);
  }, [counter]);

  const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);
  return (
    <div>
      <Header
        color="transparent"
        brand="Ridvan.org"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white"
        }}
        {...rest}
      />
      <Parallax small filter image={require("assets/img/landing-bg2.jpg")} />
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div>
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem>
            <h2>Break Out Rooms</h2>
                            <p>Here are the breakroom links:</p>
                            
                            <Button color="info" target="_blank" href="//celebrate.mostmelodious.com/Devotions">Devotional Room</Button>
                            <div>A devotional for those who would like to continue praying.</div>
                            <Button color="info" target="_blank" href="//celebrate.mostmelodious.com/Games">Game Room</Button>
                            <div>We have a couple of games to play together.</div>
                            <Button color="info" target="_blank" href="//celebrate.mostmelodious.com/RidvanLetter">Reading The Ridvan Letter</Button>
                            <div>Room for reading and studying the Ridvan letter.</div>
                            <Button color="info" target="_blank" href="//celebrate.mostmelodious.com/WelcomeChildren">Children's Room</Button>
                            <div>Children can come and learn a song and do a craft.</div>
                            <br />
                            </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
