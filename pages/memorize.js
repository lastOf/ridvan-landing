import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import SectionTabs from "pages-sections/Components-Sections/SectionTabs.js";
import Card from 'components/Card/Card.js'
import CardBody from 'components/Card/CardBody.js'
import CardHeader from 'components/Card/CardHeader.js'
import styles from "assets/jss/nextjs-material-kit/pages/landingPage.js";

// Sections for this page
import ProductSection from "pages-sections/LandingPage-Sections/ProductSection.js";
import TeamSection from "pages-sections/LandingPage-Sections/TeamSection.js";
import WorkSection from "pages-sections/LandingPage-Sections/WorkSection.js";
import SectionPills from "pages-sections/Components-Sections/SectionPills.js";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function Prepare(props) {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Ridvan.org"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
    <Parallax filter responsive image={require("assets/img/memorize.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={8}>
              <h1 className={classes.title}>Memorize</h1>
              <h3>
                Set to heart some of the Writings and Prayers of Bahá'u'lláh!
              </h3>
              
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
   {/* <SectionTabs /> */}

        <div className={classes.container}>
        <br />
          <div style={{color:'black'}}>
            <p>Here is the selection that was set aside as a suggestion for songs. Try to memorize one for the Holy Day.</p>
          <h3 >Contents</h3>
          <ol>
            <li>Sacred Verses</li>
            <ol>
              <li>The Festival of Ridván</li>
              <li>The Divine Springtime</li>
              <li>The Promised Day of God</li>
              <li>This Most Blessed of Hours</li>
            </ol>
            <li>Prayers</li>
            <ol>
              <li>I Beseech Thee</li>
              <li>This is Thy Day</li>
              <li>Sanctify Mine Eye</li>
            </ol>
          </ol>


          </div>
          <h2 style={{color:'black'}}>Selections from the Sacred Verses:</h2>
          <GridContainer>
            <GridItem>
        <Card >
      <CardHeader color="primary">
        <h3 className="springTime">The Festival of Ridván</h3>
      </CardHeader>
      <CardBody>
       <h3>from Bahá'u'lláh published in <a href="https://www.bahai.org/library/authoritative-texts/bahaullah/days-remembrance/4#693396730">Days of Remembrance</a>:</h3>
       <p>He is the Ever-Abiding.
       </p>
<p>It is the Festival of Riḍván, the vernal season wherein the Beauty of the All-Glorious was revealed betwixt earth and heaven. In this wondrous Day the gates of Paradise were flung open before the faces of all people, at the behest of Him Who is the All-Praised, and the outpourings of divine mercy rained down from the clouds of celestial favour upon His countless embodiments and manifestations in the world of being. </p> 
      </CardBody>
    </Card>   

            </GridItem>
            <GridItem>
    <Card >
      <CardHeader color="primary">
        <h3 className="springTime">The Divine Springtime</h3>
      </CardHeader>
      <CardBody>
       <h3>from Bahá'u'lláh published in <a href="https://www.bahai.org/r/208436075">Days of Remembrance</a>:</h3>
       <p>In the name of Him Who hath cast His splendour over the entire creation!
       </p>
<p>The Divine Springtime is come, O Most Exalted Pen, for the Festival of the All-Merciful is fast approaching. Bestir thyself, and magnify, before the entire creation, the name of God, and celebrate His praise, in such wise that all created things may be regenerated and made new. Speak, and hold not thy peace. The day-star of blissfulness shineth above the horizon of Our name, the Blissful, inasmuch as the kingdom of the names of God hath been adorned with the ornament of the name of thy Lord, the Creator of the heavens. Arise before the nations of the earth, and arm thyself with the power of this Most Great Name, and be not of those who tarry.</p> 
      </CardBody>
    </Card>   

            </GridItem>
            <GridItem>
    <Card >
      <CardHeader color="primary">
        <h3 className="springTime">The Promised Day of God</h3>
      </CardHeader>
      <CardBody>
       <h3>from Bahá'u'lláh published in <a href="https://www.bahai.org/r/441034330">Days of Remembrance</a>:</h3>
       <p>The promised Day of God is come! He Who is the Manifestation of the Adored One hath been established upon the throne of His name, the All-Loving, and the sun of His bounty hath cast its rays upon the seeing and seen alike. Wherefore renounce ye, O denizens of the realms of limitation, that which ye possess, adorn your temples with His glorious vesture, and behold with untainted vision Him Who is the luminous Beauty of God seated upon the throne of glory in His transcendent, His almighty and all-subduing sovereignty. All praise be to the Best-Beloved, Who hath revealed His hidden beauty with such manifest authority!
       </p>
      </CardBody>
    </Card>   

            </GridItem>
            <GridItem>
    <Card >
      <CardHeader color="primary">
        <h3 className="springTime">This Most Blessed of Hours</h3>
      </CardHeader>
      <CardBody>
       <h3>from Bahá'u'lláh published in <a href="https://www.bahai.org/r/168685065">Days of Remembrance</a>:</h3>
       <p>The day-star of words, dawning above the horizon of the utterance of Him Who is the Lord of all names and attributes, hath, at this most blessed of hours, shone forth in all truth with the splendours of the light of God. The spirit of understanding, flowing from the Pen of the All-Glorious, hath, by virtue of His grace, been conferred upon all created things. The mystery of all mysteries, emerging from behind the veils of concealment, hath, in very truth, been revealed to the righteous, as bidden by God, the Almighty, the Unconstrained.
       </p>
      </CardBody>
    </Card>   

            </GridItem>
            <h2 style={{color:'black'}}>Prayers</h2>
            <GridItem>
    <Card >
      <CardHeader color="primary">
        <h3 className="springTime">I Beseech thee</h3>
      </CardHeader>
      <CardBody>
       <h3><a href="https://www.bahai.org/r/301701477">Revealed</a> by Bahá'u'lláh:</h3>
       <p>O God, my God! Glory be to Thee for having guided me unto the horizon of Thy Revelation, illumined me with the splendours of the light of Thy grace and mercy, caused me to speak forth Thy praise, and given me to behold that which hath been revealed by Thy Pen.
       </p>
       <p>I beseech Thee, O Thou the Lord of the kingdom of names and Fashioner of earth and heaven, by the rustling of the Divine Lote-Tree and by Thy most sweet utterance which hath enraptured the realities of all created things, to raise me up in Thy Name amidst Thy servants. I am he who hath sought in the daytime and in the night season to stand before the door of Thy bounty and to present himself before the throne of Thy justice. O Lord! Cast not away him who hath clung to the cord of Thy nearness, and deprive not him who hath directed his steps towards Thy most sublime station, the summit of glory, and the supreme objective—that station wherein every atom crieth out in the most eloquent tongue, saying: “Earth and heaven, glory and dominion are God’s, the Almighty, the All-Glorious, the Most Bountiful!</p>
      </CardBody>
    </Card>   

            </GridItem>
            <GridItem>
    <Card >
      <CardHeader color="primary">
        <h3 className="springTime">This is Thy Day</h3>
      </CardHeader>
      <CardBody>
       <h3><a href="https://www.bahai.org/r/511039431">Revealed</a> by Bahá'u'lláh:</h3>
       <p>O my God! O my God! I testify that this is Thy Day which hath been mentioned in Thy Books, Thy Epistles, Thy Psalms and Thy Tablets. In it Thou hast manifested that which was hidden in Thy Knowledge and stored up in the repositories of Thine unfailing protection. I beseech Thee, O Lord of the world, by Thy Most Great Name whereby the limbs of the people were shaken, to assist Thy servants and Thy handmaidens to become steadfast in Thy Cause and to arise in Thy service.
       </p>
       <p>Verily, Thou art potent to do whatsoever Thou willest, and in Thy grasp are the reins of all things. Thou protectest whomsoever Thou willest through Thy Power and Dominion. And verily, Thou art the Almighty, the All-Subduing, the Most Powerful.</p>
      </CardBody>
    </Card>   

            </GridItem>
            <GridItem>
    <Card >
      <CardHeader color="primary">
        <h3 className="springTime">Sanctify Mine Eye</h3>
      </CardHeader>
      <CardBody>
       <h3><a href="https://www.bahai.org/r/533738942">Revealed</a> by Bahá'u'lláh:</h3>
       <p>Praised be Thou, O Lord my God! Sanctify mine eye, and mine ear, and my tongue, and my spirit, and my heart, and my soul, and my body, and mine entire being from turning unto anyone but Thee. Give me then to drink from the cup that brimmeth with the sealed wine of Thy glory.
       </p>
      </CardBody>
    </Card>   

            </GridItem>
          </GridContainer>
        </div>
      </div>
      <Footer />
    </div>
  );
}
