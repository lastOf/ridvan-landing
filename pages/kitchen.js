import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import SectionTabs from "pages-sections/Components-Sections/SectionTabs.js";
import Card from 'components/Card/Card.js'
import CardBody from 'components/Card/CardBody.js'
import CardHeader from 'components/Card/CardHeader.js'
import styles from "assets/jss/nextjs-material-kit/pages/landingPage.js";

import tStyles from "assets/jss/nextjs-material-kit/pages/componentsSections/typographyStyle.js";

// Sections for this page
import ProductSection from "pages-sections/LandingPage-Sections/ProductSection.js";
import TeamSection from "pages-sections/LandingPage-Sections/TeamSection.js";
import WorkSection from "pages-sections/LandingPage-Sections/WorkSection.js";
import SectionPills from "pages-sections/Components-Sections/SectionPills.js";

import bread from 'assets/img/bread.jpg'


const dashboardRoutes = [];

const useStyles = makeStyles(styles);
let useTStyles = makeStyles(tStyles);
export default function Prepare(props) {
  const classes = useStyles();
  let tClasses = useTStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Ridvan.org"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
    <Parallax filter responsive image={require("assets/img/kitchen.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>Cook Something Sweet!</h1>
              <h3>
                A few ideas for the kitchen during this Most Great Festival!
              </h3>
              
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
   {/* <SectionTabs /> */}
        <div className={classes.container}>
        <div className={classes.section}>
<br />
   <div style={{color:'black'}}>
          <h3 >Contents</h3>
          <ol>
            <li>Bake a Loaf of Bread</li>
            <li>Make Rosewater</li>
          </ol>
          </div>

        <GridContainer>
            <GridItem xs={12} sm={12} md={8}>
        <Card >
      <CardHeader color="primary">
        <h2 className="springTime">Bake a Loaf of Bread</h2>
      </CardHeader>
      <CardBody>
      <img
                src={bread}
                alt="..."
                className={
                  tClasses.imgRaised +
                  " " +
                  tClasses.imgRoundedCircle +
                  " " +
                  tClasses.imgFluid}
                 />
          <h3>How long will it take?</h3>
          <p>The bread has to rise for about 8-12 hours overnight. It bakes for 40 minutes. Preparation takes 15 minutes at most.</p>
       <h3>Ingredients:</h3> 
       <div>
           <p>
           Water <b>1 1/2 Cups</b> 
           </p>
           <p>
           Yeast  <b>1/2 Teaspoon</b> 
           </p>
           <p>
           Salt  <b>1/2 Teaspoon</b> 
           </p>
           <p>
           Flour  <b>3 Cups</b> 
           </p>
           <p>
           <i>Optional: </i> Sugar <b>1/8 Cup</b> (Other sweeteners are fine too, however I haven't tested a measurement for them yet)
           </p>
           <p>
           Mixing Bowl, a fork, and a cover (could use a plate)
           </p>
           <p>
           Baking Dish with a lid
           </p>
       </div>
       <h3>Mixing Instructions:</h3>
       <ol>
           <li>
           Pour <b>1 1/2 cups</b> of water into the mixing bowl.
           </li>
           <li>
           Add <b>1/2 Teaspoon</b> Yeast to the water. It is important for the yeast to enter the water before the other ingredients so it has time to activate.
           </li>
           <li>
            Add <b>1/2 Teaspoon</b> of Salt to the water.
           </li>
           <li>
           <i>Optionally,</i>  you can add a sweetener. Any more than <b>1/8 Cup</b> of sugar will make the bread distinctly sweet, more than bread usually is.
           </li>
           <li>
           Add <b>3 Cups</b> of Flour.
           </li>
           <li>
           Mix the ingredients together until they create a doughy ball, try to get all the flour inside.
           </li>
           <li>
               Cover the dough and let it sit overnight
           </li>
       </ol>
       <h3>Baking Instructions:</h3>
       <ol>
       <li>
           Pre-heat oven to <b>425</b> with the baking dish <b>inside</b>.
           </li>
           <li>
           <b>Once the oven is preheated</b>, take out the dish carefully, sprinkly a small layer of flour on the bottom.
           </li>
           <li>
           Cover your hands with flour, so the dough doesnt stick.
           </li>
           <li>
           Scoop all of the dough out into the baking dish.
           </li>
           <li>
            Put the lid <b>on</b>.
           </li>
           <li>
           Bake the bread for <b>25 minutes</b>, with the lid on.
           </li>
           <li>
           Remove the lid and bake for another <b>15 minutes</b>.
           </li>
           <li>
           Remove from the oven and let it cool for about 15 minutes before cutting.
           </li>
           <li>
               Enjoy!
           </li>
       </ol>
      </CardBody>
    </Card>
    </GridItem>
            <GridItem xs={12} sm={12} md={8}>
    <Card >
      <CardHeader color="primary">
        <h2 className="springTime">Make Rosewater</h2>
      </CardHeader>
      <CardBody>
      {/* <img
                src={bread}
                alt="..."
                className={
                  tClasses.imgRaised +
                  " " +
                  tClasses.imgRoundedCircle +
                  " " +
                  tClasses.imgFluid}
                 /> */}
          <h3>How long will it take?</h3>
          <p>Under thirty minutes. Mostly waiting for it too cool.</p>
       <h3>Materials and Ingredients:</h3> 
       <div>
           <p>
           Dried Rose Petals <b>1/4 Cup</b> 
           </p>
           <p>
           Hot Water  <b>1 1/4 Cups</b> (filtered or distilled if available)
           </p>
           <p>
           Jars <b>two of them</b> and one lid
           </p>
           <p>
           Strainer 
           </p>
       </div>
       <h3>Instructions:</h3>
       <ol>
           <li>
           Place the dried rose petals into a jar.
           </li>
           <li>
           Pour <b>1 1/4 Cups</b> of hot water into the jar with the petals. The water should <b>not be boiling</b>.
           </li>
           <li>
            Add a cover and let the water cool to room temperature. Wait around <b>15 minutes</b>.
           </li>
           <li>
             Strain the water into the empty jar, <b>removing the rose petals</b>.
           </li>
           <li>
           Seal the rosewater and store it in the fridge, it will be good for about a week.
           </li>
       </ol>
      </CardBody>

    </Card> 
            </GridItem>  
    </GridContainer>
        </div>
      </div>
      </div>
      <Footer />
    </div>
  );
}
