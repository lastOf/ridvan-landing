import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// import 'assets/css/myCss.css';
// @material-ui/icons
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from "@chakra-ui/core";

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import Info from "components/Typography/Info.js";


import styles from "assets/jss/nextjs-material-kit/pages/landingPage.js";

// Sections for this page
import ProductSection from "pages-sections/LandingPage-Sections/ProductSection.js";
import TeamSection from "pages-sections/LandingPage-Sections/TeamSection.js";
import WorkSection from "pages-sections/LandingPage-Sections/WorkSection.js";
import SectionPills from "pages-sections/Components-Sections/SectionPills.js";
import { LineWeight } from "@material-ui/icons";
import { Grid } from "@material-ui/core";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);



export default function LandingPage(props) {
  const classes = useStyles();
  const { ...rest } = props;


  
  let timezones = [
    '9am Chicago time',
    '4am Mafikeng time',
    '5pm Kuwait time',
    '10am Georgia time',
    '7am Phoenix time',
    '10pm Shanghai time',
    '10am New York time',
  ]

  let mystyle = {
    fontWeight: 'bold',
  }

  let [zone, setZone] = React.useState('10am South Carolina Time')
  let [counter, setCounter] = React.useState(0);
  let [started, setStarted] = React.useState(false)
  let linkMaker = () => {if(started){return {
    text: "Join the Meeting!",
    link: "//celebrate.mostmelodious.com/HappyRidvan",
    color: "success",
    rooms: "/rooms",
    roomsText: "Join a Room!"
  } } else {
    return {
      text: "The Celebration Has not Started Yet",
      link: "",
      color: "info",
      rooms: "",
      roomsText: "Rooms Open after Sharing"
    }
  }
}
let joinLink = linkMaker()

  const [isOpen, setIsOpen] = React.useState();
  const onClose = () => setIsOpen(false);
  const cancelRef = React.useRef();

  React.useEffect(() => {
    const interval = setInterval(() => {
      setZone(timezones[counter]);
      setCounter(counter => counter + 1);
      if(new Date()>new Date("tue Apr 28 2020 09:50:00 GMT-0400 (Eastern DaylightTime")){
        setStarted(true)
      }
      if (counter+1 == timezones.length) {
        setCounter(0);
        // clearInterval(inst); // uncomment this if you want to stop refreshing after one cycle
      }
    }, 3000);
    return () => clearInterval(interval);
  }, [counter]);


  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Ridvan.org"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
    <Parallax filter responsive image={require("assets/img/landing-bg2.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>Happy Ridván!</h1>
              <h3>
      Come join an online Ridván Celebration at <span
      style={mystyle}
      className="timezone">{zone}</span>, April 28th!
              </h3>
              <br />
              <Button
                color="success"
                size="md"
                href="/agenda"
                rel="noopener noreferrer"
              >
                Check out the Agenda
              </Button>
            </GridItem>
            <GridItem>
            <Button
            size="lg"
            color="primary" onClick={() => setIsOpen(true)}>
        Join the Celebration!
      </Button>

      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={onClose}
      >
        <AlertDialogOverlay />
        <AlertDialogContent>
          <AlertDialogHeader fontSize="lg" fontWeight="bold">
            Welcome!
          </AlertDialogHeader>

          <AlertDialogBody>
            <p>We are so happy you are here!</p>
            <p>At the start, everyone will have their <b>mics and cameras off.</b></p>
            <p>When you enter, if devotions have not started, you can turn your mic and camera on to greet people, but then please turn it back off afterward. For large numbers this will ensure everyone is able to participate. </p>
            <h5>Also note: The meeting will work best from <b>Firefox</b> and <b>Chrome</b> browsers.</h5>
            <p>After the celebration, come back to the home page to sign the guestbook!</p>
          </AlertDialogBody>

          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              Cancel
            </Button>
            <Button color={joinLink.color} href={joinLink.link} onClick={onClose} ml={3}>
             {joinLink.text}
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
            </GridItem>
            <GridItem>
            <Button href="https://docs.google.com/forms/d/e/1FAIpQLSc5ZH73cCOMB-j2H9GptowcNPyRM6_Cn70VOu4GcOBOxUPrWg/viewform?usp=sf_link" color="rose">
              Sign the Guestbook!
            </Button>
            </GridItem>
            <GridItem>
            <Button href={joinLink.rooms} color={joinLink.color}>
              {joinLink.roomsText}
            </Button>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div style={{color:"black"}} className={classes.container}>
          <ProductSection />
          <SectionPills />
          <GridContainer>

          <GridItem>
                <h3>About</h3>
                <p>Hello friends, Allah'u'Abha! Happy Ridván!</p>
                <p>This is our effort to contribute to a joyful Ridván during the widespread tumult, and test our servers 😆.</p>
                <p>We especially want to accomadate children with this event and these resources, so we have tried to put things together thoughtfully. They prefer things that way. If you have any suggestions, please reach out.</p>
                <p>We are looking forward to seeing your smiling faces and sharing a little bit of time together!</p>
                <p>Allah'u'Abha!</p>
                <div>-Quddús</div>
              </GridItem>
          </GridContainer>
          <br/>
        </div>
      </div>
      <Footer />
    </div>
  );
}
