import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import SectionTabs from "pages-sections/Components-Sections/SectionTabs.js";
import Card from 'components/Card/Card.js'
import CardBody from 'components/Card/CardBody.js'
import CardHeader from 'components/Card/CardHeader.js'
import styles from "assets/jss/nextjs-material-kit/pages/landingPage.js";

// Sections for this page
import ProductSection from "pages-sections/LandingPage-Sections/ProductSection.js";
import TeamSection from "pages-sections/LandingPage-Sections/TeamSection.js";
import WorkSection from "pages-sections/LandingPage-Sections/WorkSection.js";
import SectionPills from "pages-sections/Components-Sections/SectionPills.js";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function Prepare(props) {
  const classes = useStyles();
  const { ...rest } = props;
  
  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Ridvan.org"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
    <Parallax filter responsive image={require("assets/img/activities.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={8}>
              <h1 className={classes.title}>Ridván Activities</h1>
              <h3>
                Some printable activities to enjoy during Ridván!
              </h3>
              
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>

        <div className={classes.container}>
          <GridContainer>
          <GridItem xs={12} sm={12} md={8}>
    <Card >
      <CardHeader color="primary">
        <h2 className="springTime">Coloring Sheets</h2>
      </CardHeader>
      <CardBody>
        <Button color="primary" href="/roan.pdf" target="_blank">Red Roan</Button>
        <Button color="primary" href="/plantSeed.pdf" target="_blank">Plant a seed</Button>
        <Button color="primary" href="/gate.pdf" target="_blank">Gateway to the Gardens</Button>
        <Button color="primary" href="/Tree.pdf" target="_blank">Grow a Tree</Button>
        <Button color="primary" href="/flower.pdf" target="_blank">Ridvan Rose</Button>
        <Button color="primary" href="/flowers2.pdf" target="_blank">Ridvan Flowers</Button>
        <Button color="primary" href="/ridvanRoan.pdf" target="_blank">Ridvan Roan</Button>
      </CardBody>

    </Card> 
            </GridItem>  
            <GridItem xs={12} sm={12} md={8}>
    <Card >
      <CardHeader color="primary">
        <h2 className="springTime">Coloring Book</h2>
      </CardHeader>
      <CardBody>
        <h3>Exploring Spiritual Reality: Prayer</h3>
        <p>A coloring book exploring the concept of prayer, drawn from the themes in Ruhi Book 1.</p>
        <p>(The front and back covers are the last two pages, this makes it easier to print those seperately if you want to use cardstock for the covers. Once in order, staple or bind along the top edge like a flipbook and enjoy.)</p>
        <Button color="primary" href="/prayer.pdf" target="_blank">Download</Button>

      </CardBody>

    </Card> 
            </GridItem>  
          <GridItem xs={12} sm={12} md={8}>
    <Card >
      <CardHeader color="primary">
        <h2 className="springTime">Crosswords</h2>
      </CardHeader>
      <CardBody>
        <Button color="primary" href="/crossw.pdf" target="_blank">Ridvan Garden Crossword</Button>
        <Button color="primary" href="/crossw2.pdf" target="_blank">Ridvan Quote Crossword</Button>
        
      </CardBody>

    </Card> 
            </GridItem> 
          </GridContainer>
        </div>
      </div>
      <Footer />
    </div>
  );
}
