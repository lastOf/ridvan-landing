import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import styles from "assets/jss/nextjs-material-kit/components/typographyStyle.js";

const useStyles = makeStyles(styles);

export default function Info(props) {
  const classes = useStyles();
  const { children } = props;
  return (
    <span  className={classes.defaultFontStyle + " " + classes.infoText + "  timezone"}>
      {children}
    </span>
  );
}

Info.propTypes = {
  children: PropTypes.node
};
