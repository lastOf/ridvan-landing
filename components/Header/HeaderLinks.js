/*eslint-disable*/
import React from "react";
import Link from "next/link";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

// @material-ui/icons
import { Apps, CloudDownload } from "@material-ui/icons";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import ListAltIcon from '@material-ui/icons/ListAlt';


// core components
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/nextjs-material-kit/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();
  return (
    <List className={classes.list}>
          <ListItem className={classes.listItem}>
            <Button
              href="/agenda"
              color="transparent"
              className={classes.navLink}
            >
              <ListAltIcon className={classes.icons} /> Agenda
            </Button>
            <Button
              href="/song"
              color="transparent"
              className={classes.navLink}
            >
               Make a Song
            </Button>
            <Button
              href="/story"
              color="transparent"
              className={classes.navLink}
            >
               Tell a Story
            </Button>
            <Button
              href="/memorize"
              color="transparent"
              className={classes.navLink}
            >
               Memorization
            </Button>
            <Button
              href="/kitchen"
              color="transparent"
              className={classes.navLink}
            >
               Kitchen
            </Button>
            <Button
              href="/activities"
              color="transparent"
              className={classes.navLink}
            >
               Activities
            </Button>

          </ListItem>
      {/* <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          navDropdown
          buttonText="Prepare"
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          buttonIcon={Apps}
          dropdownList={[
            <Link href="/song">
              <a className={classes.dropdownLink}>Make a Song</a>
            </Link>,
 <Link href="/story">
 <a className={classes.dropdownLink}>Tell a Story</a>
</Link>,
 <Link href="/memorize">
 <a className={classes.dropdownLink}>Memorization</a>
</Link>,
 <Link href="/kitchen">
 <a className={classes.dropdownLink}>Culinary</a>
</Link>,
 <Link href="/activities">
 <a className={classes.dropdownLink}>Activities</a>
</Link>
          ]}
        />
      </ListItem> */}
      </List>
  );
}
